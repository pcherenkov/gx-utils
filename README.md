# README #

### What is this repository for? ###

This repository is for the public-domain code within the GigaX line of products.
The line (which at this point) includes: udpxy, Gigapxy, GigA+, RoWAN and GigaTools.

All the code provided therein can be independenly compiled under a supported
Linux (distro) or FreeBSD.

### What's in there?

src:
-------
mcprobe.c - test visibility/throughput of a multicast channel;
udpipe.c  - copy UDP traffic from point A to point B (UDP-to-UDP);
toudp.c   - copy text to UDP, single message/packet per line;
stt.c     - copy file to STDOUT in random-sized chunks, with random pauses between chunks.

script:
-------
pmon.sh   - monitor process's memory consumption (using ps(1)).

### How do I get set up? ###

All you need is an ANSI C98-compliant version of gcc or clang. Linux before 2.6
or FreeBSD <9.x may be too old for some of the code. Some latest compilers may
yield warnings/errors for the older code, please report such cases to the author.

Also, not all utilities may have been built & tested under ALL the supported distros,
if something does not build, yell (and kindly let me know).

To build a single-file module (module.c), run:

gcc -W -Wall -Wextra -g3 -o module-exec module.c

### Contribution guidelines ###

You're welcome to fork, report errors, suggest merges, etc. Sky is the limit.

### Who do I talk to? ###

Pavel Cherenkov

## How to contact?

Contact via Gigapxy Telegram chat (find reference in Google+ Gigapxy community)
or by email (support [at] gigapxy.com).

# _EOF_

