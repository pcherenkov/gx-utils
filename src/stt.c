/* @(#) stt: write to STDOUT file contents in random-sized chunks, with random delays. */

#include <sys/types.h>
#include <stdint.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/stat.h>
#include <fcntl.h>

#include <assert.h>
#include <errno.h>


/**
 * Data, constants:
 */

enum {
    D_MIN_MS        = 0,
    D_MAX_MS        = 10000,
    D_MIN_BYTES     = 16,
    D_MAX_BYTES     = 163840,
    D_SEED          = 0
};

/**
 * Functions:
 */

static void
usage(const char *app)
{
    fprintf(stderr, "Usage: %s path [min-ms max-ms min-bytes max-bytes seed]\n", app);
    fprintf(stderr, "ms: %d..%d, bytes: %d..%d\n\n", D_MIN_MS, D_MAX_MS, D_MIN_BYTES, D_MAX_BYTES);
    exit(1);
}


int
main(int argc, char *const argv[])
{
    int rc = 0, sfd = -1;
    const char *fpath = NULL;
    int min_ms = D_MIN_MS, max_ms = D_MAX_MS, min_bytes = D_MIN_BYTES, max_bytes = D_MAX_BYTES;
    int sz_delta = 0, pause_delta = 0, seed = D_SEED, in_debug = 0;

    static char buf[D_MAX_BYTES];

    if (argc <= 1)
        usage(argv[0]);

    fpath = argv[1];
    if (argc <= 2)
        goto ioloop;

    min_ms = atoi(argv[2]);
    if (min_ms < D_MIN_MS || min_ms > max_ms) {
        fprintf(stderr, "Bad min-ms: %s\n", argv[2]);
        rc = 2; goto done;
    }
    if (argc <= 3)
        goto ioloop;

    max_ms = atoi(argv[3]);
    if (max_ms < min_ms || max_ms > D_MAX_MS) {
        fprintf(stderr, "Bad max-ms: %s\n", argv[3]);
        rc = 2; goto done;
    }
    if (argc <= 4)
        goto ioloop;

    min_bytes = atoi(argv[4]);
    if (min_bytes < D_MIN_BYTES || min_bytes > max_bytes) {
        fprintf(stderr, "Bad min-bytes: %s\n", argv[4]);
        rc = 2; goto done;
    }
    if (argc <= 5)
        goto ioloop;

    max_bytes = atoi(argv[5]);
    if (max_bytes < min_bytes || max_bytes > D_MAX_BYTES) {
        fprintf(stderr, "Bad max-bytes: %s\n", argv[5]);
        rc = 2; goto done;
    }
    if (argc <= 6)
        goto ioloop;

    seed = atoi(argv[6]);
    if (seed <= 0) {
        fprintf(stderr, "Invalid seed: %s\n", argv[6]);
        rc = 2; goto done;
    }

ioloop:
    in_debug = NULL != getenv("STT_DEBUG");

    sfd = open(fpath, O_RDONLY);
    if (-1 == sfd) {
        int err = errno;
        fprintf(stderr, "open(%s,..) ERROR(%d): %s\n", fpath, err, strerror(err));
        rc = 2; goto done;
    }

    if (seed)
        srand((u_int)seed);

    sz_delta = max_bytes - min_bytes + 1;
    pause_delta = max_ms - min_ms + 1;

    fprintf(stderr, "IN: %s, ms: %d..%d, bytes: %d..%d\n", fpath, min_ms, max_ms,
        min_bytes, max_bytes);

    if (in_debug) fputs("\n", stderr);
    for (;;) {
        ssize_t nrd = -1, nwr = -1;
        int chunk_len = min_bytes + rand() % sz_delta,
            pause_ms = min_ms + rand() % pause_delta;

        assert(chunk_len <= D_MAX_BYTES && pause_ms <= D_MAX_MS);
        nrd = read(sfd, buf, (size_t)chunk_len);
        if (0 == nrd)
            break;
        if (-1 == nrd) {
            int err = errno;
            fprintf(stderr, "read(fd%d, ..,%d) ERROR(%d): %s\n", sfd, chunk_len,
                err, strerror(err));
            rc = 2; goto done;
        }

        (void) usleep(pause_ms * 1000);

        nwr = write(1, buf, (size_t)nrd);
        if (nwr <= 0) {
            int err = errno;
            fprintf(stderr, "write(1, ..,%zd) ERROR(%d): %s\n", nrd,
                err, strerror(err));
            rc = 2; goto done;
        }
        assert(nrd == nwr);

        if (in_debug) {
            fprintf(stderr, "[%zd:%dms] ", nwr, pause_ms);
        }
    }
    if (in_debug) fputs("\n", stderr);

done:
    if (sfd > 0) {
        (void) close(sfd); sfd = -1;
    }

    return rc;
}



/* __EOF__ */

