/* @(#) multicast-traffic bridge utility
 */

#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <net/if.h>
#include <netinet/in.h>
#include <sys/time.h>
#include <signal.h>

#ifdef _DEBUG
    #define TRACE( expr ) expr
#else
    #define TRACE( expr ) ((void)0)
#endif

#define IPv4_STRLEN     20
#define PORT_STRLEN     6

#define ETHERNET_MTU 1500
#define RC_TIMEOUT   -100

static volatile sig_atomic_t g_quit = 0;

/* return 1 if the application must gracefully quit
 */
static sig_atomic_t
must_quit() { return g_quit; }

static FILE* g_flog = NULL;

/* handler for signals requestin application exit
 */
static void
handle_quitsigs(int signo)
{
    g_quit = (sig_atomic_t)1;
    (void) &signo;

    TRACE( (void)fprintf( g_flog,
                "*** Caught SIGNAL %d in process=[%d] ***\n",
                signo, getpid()) );
    return;
}


static int
parse_aport(const char *aport, struct sockaddr_in *sa)
{
    char ipaddrp[ IPv4_STRLEN + PORT_STRLEN ] = { 0 },
         *p = NULL;
    int port = 0;

    memset(sa, 0, sizeof(*sa));
    sa->sin_family = AF_INET;

    (void) strncpy(ipaddrp, aport, sizeof(ipaddrp) - 1);
    if( NULL == (p = strtok(ipaddrp, ":") ))
        return -1;

    if( 0 == inet_aton(p, &sa->sin_addr) )
        return -1;

    if( NULL == (p = strtok( NULL, ":" )) )
        return -1;

    port = atoi(p);
    if( port < 1 ) {
        (void) fprintf(g_flog, "Invalid port value=[%d]\n",
                (int)port);
        return -1;
    }

    sa->sin_port = htons((short)port);
    return 0;
}


static int
is_multicast_addr(const char *aport)
{
    struct sockaddr_in sa;
    if (0 != parse_aport(aport, &sa)) {
        return -1;
    }

    return IN_MULTICAST(ntohl(sa.sin_addr.s_addr));
}


static int
setup_udp_socket(const char *ipstr, int inbound)
{
    int sockfd = -1, fd = -1, rc = 0;
    struct sockaddr_in sa;

    assert(ipstr);

    TRACE( (void)fprintf( g_flog, "%s: [%s]\n", __func__, ipstr) );

    if (0 != parse_aport(ipstr, &sa))
        return -1;

    do {
        fd = socket( AF_INET, SOCK_DGRAM, 0 );
        if( -1 == fd ) {
            perror( "socket" );
            break;
        }

        if (inbound) {
            if( 0 != bind( fd, (struct sockaddr*)&sa, sizeof(sa)) ) {
                perror( "bind" );
                break;
            }
        }

        if (0 == inbound) {
            rc = connect(fd, (struct sockaddr*)&sa, sizeof(sa));
            if (rc) {
                perror("connect");
                break;
            }
        }

        sockfd = fd;

    } while (0);

    TRACE( fprintf(g_flog, "%s: %s socket fd%d\n", __func__, inbound ? "input" : "output", sockfd) );
    return sockfd;
}


static int
setup_mcast_socket( const char* mifcstr,
                    const char* ipstr,
                    int         inbound)
{
    int sockfd = -1, fd = -1, rc = 0;
    struct sockaddr_in sa, msa;
    struct in_addr mifaddr;
    struct ip_mreq mreq;
    int ON = 1;
    socklen_t len = sizeof(msa);

    assert( mifcstr && ipstr );

    TRACE( fprintf( g_flog, "%s [%s] on ifc=[%s]\n", __func__, ipstr, mifcstr ) );

    memset( &msa, 0, sizeof(msa) );

    if (0 != parse_aport(ipstr, &sa))
        return -1;

    if( 0 == inet_aton( mifcstr, &mifaddr ) )
        return -5;

    do {
        fd = socket( AF_INET, SOCK_DGRAM, 0 );
        if( -1 == fd ) {
            perror( "socket" );
            break;
        }

        ON = 1;
        rc = setsockopt( fd, SOL_SOCKET, SO_REUSEADDR,
                         &ON, sizeof(ON) );
        if( 0 != rc ) {
            perror( "setsockopt SO_REUSEADDR" );
            break;
        }

        if (0 == inbound)
            break;

        if( 0 != bind( fd, (struct sockaddr*)&sa, sizeof(sa)) ) {
            perror( "bind" );
            break;
        }

        (void) memset( &mreq, 0, sizeof(mreq) );
        (void) memcpy( &mreq.imr_multiaddr, &(sa.sin_addr),
                sizeof(struct in_addr) );

        (void) memcpy( &mreq.imr_interface, &mifaddr,
                sizeof(struct in_addr) );

        rc = setsockopt( fd, IPPROTO_IP, IP_ADD_MEMBERSHIP,
                &mreq, sizeof(mreq) );
        if( 0 != rc ) {
            perror( "setsockopt IP_ADD_MEMBERSHIP" );
            break;
        }

        sockfd = fd;

    } while(0);


    if( 0 != rc ) {
        if( sockfd >= 0 ) {
            (void) close( sockfd );
        }
        return -1;
    }

    (void) memset( &msa, 0, sizeof(msa) );
    rc = getsockname( sockfd, (struct sockaddr*)&msa, &len );
    if( 0 != rc ) perror( "getsockname(msa)" );

    fprintf( g_flog, "%s: fd%d %s [%s:%d] on ifc=[%s]\n", __func__,
                sockfd, inbound ? "inbound" : "outbound",
                inet_ntoa( msa.sin_addr ), ntohs( msa.sin_port ), mifcstr);
    return sockfd;
}


static void
close_mcast_socket( int msockfd, const char* mifcstr )
{
    struct ip_mreq mreq;
    struct sockaddr_in addr;
    struct in_addr  mifaddr;
    socklen_t len = sizeof( addr );
    int rc = 0;
    char ipaddr[ IPv4_STRLEN ] = {0};

    assert( (msockfd > 0) && mifcstr );

    if( msockfd <= 0 ) return;

    if( 0 == inet_aton( mifcstr, &mifaddr ) ) {
        fprintf( g_flog, "%s: inet_aton error for [%s]\n",
                __func__, mifcstr );
        return;
    }

    (void) memset( &mreq, 0, sizeof(mreq) );
    (void) memcpy( &mreq.imr_interface, &mifaddr,
                sizeof(struct in_addr) );

    (void) memset( &addr, 0, sizeof(addr) );
    rc = getsockname( msockfd, (struct sockaddr*)&addr, &len );
    if( 0 != rc ) {
        perror( "getsockname" );
    }
    else {
        strncpy( ipaddr, inet_ntoa( addr.sin_addr ), sizeof(ipaddr) - 1 );
        (void) memcpy( &mreq.imr_multiaddr, &addr.sin_addr,
                    sizeof(struct in_addr) );
    }

    rc = setsockopt( msockfd, IPPROTO_IP, IP_DROP_MEMBERSHIP,
                    &mreq, sizeof(mreq) );
    if( 0 != rc ) {
        perror( "setsockopt IP_DROP_MEMBERSHIP" );
    }

    rc = close( msockfd );
    if( 0 != rc ) {
        perror( "close (msockfd)" );
    }

    TRACE( (void)fprintf( g_flog, "Disconnected from channel=[%s:%d]\n",
                ipaddr, (int)ntohs(addr.sin_port)) );
    return;
}


static int
renew_mcast_subscr( int msockfd, const char* mifcstr )
{
    struct ip_mreq mreq;
    struct sockaddr_in addr;
    struct in_addr  mifaddr;
    socklen_t len = sizeof( addr );
    int rc = 0;
    char ipaddr[ IPv4_STRLEN ] = {0};

    assert( (msockfd > 0) && mifcstr );

    if( 0 == inet_aton( mifcstr, &mifaddr ) ) {
        fprintf( g_flog, "%s: inet_aton error for [%s]\n",
                __func__, mifcstr );
        return -1;
    }

    (void) memset( &mreq, 0, sizeof(mreq) );
    (void) memcpy( &mreq.imr_interface, &mifaddr,
                sizeof(struct in_addr) );

    (void) memset( &addr, 0, sizeof(addr) );
    rc = getsockname( msockfd, (struct sockaddr*)&addr, &len );
    if( 0 != rc ) {
        perror( "getsockname" );
        return -1;
    }

    strncpy( ipaddr, inet_ntoa( addr.sin_addr ), sizeof(ipaddr) - 1 );
    (void) memcpy( &mreq.imr_multiaddr, &addr.sin_addr,
                sizeof(struct in_addr) );

    rc = setsockopt( msockfd, IPPROTO_IP, IP_DROP_MEMBERSHIP,
                    &mreq, sizeof(mreq) );
    if( 0 != rc ) {
        perror( "setsockopt IP_DROP_MEMBERSHIP" );
    }

    /* re-populate mreq in case it was reset by prior setsockopt */
    (void) memset( &mreq, 0, sizeof(mreq) );
    (void) memcpy( &mreq.imr_interface, &mifaddr,
                sizeof(struct in_addr) );
    (void) memcpy( &mreq.imr_multiaddr, &addr.sin_addr,
                sizeof(struct in_addr) );

    rc = setsockopt( msockfd, IPPROTO_IP, IP_ADD_MEMBERSHIP,
            &mreq, sizeof(mreq) );
    if( 0 != rc ) {
        perror( "setsockopt IP_ADD_MEMBERSHIP" );
        return -1;
    }

    TRACE( (void)fprintf( g_flog, "Multicast subscription renewed\n" ) );
    return 0;
}


static int
set_rtmout( int sockfd, short sec, short usec )
{
    struct timeval tv;
    int rc = 0;

    tv.tv_sec   = sec;
    tv.tv_usec  = usec;

    rc = setsockopt( sockfd, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv) );
    if( -1 == rc ) {
        perror( "setsockopt SO_RCVTIMEIO" );
    }

    return rc;
}


static int
tvalcmp( const struct timeval* tv1, const struct timeval* tv2 )
{
    assert( tv1 && tv2 );

    if( tv1->tv_sec > tv2->tv_sec ) return 1;
    if( tv1->tv_sec < tv2->tv_sec ) return -1;

    if( tv1->tv_usec > tv2->tv_usec ) return 1;
    if( tv1->tv_usec < tv2->tv_usec ) return -1;

    return 0;
}


static int
relay(int msockfd, int usockfd, int mcast_out, const struct sockaddr *outaddr)
{
    char buf[ ETHERNET_MTU ];
    struct timeval tvb, tva, dtv,
                   min_dtv, max_dtv;
    time_t ltm, tmnow;
    char str_tmnow[ 32 ];

    int rc = 0, in_fd = -1, out_fd = -1;
    ssize_t nread = -1, nsent = -1, sum_read = 0, bytesec;
    size_t packets = (size_t)0, len;

    assert( msockfd > 0 );
    ltm = tmnow = time(NULL);

    in_fd = mcast_out  ? msockfd : usockfd;
    out_fd = mcast_out ? usockfd : msockfd;
    fprintf( g_flog, "%s: in:fd%d, out:df%d \n", __func__, in_fd, out_fd);

    timerclear( &min_dtv );
    timerclear( &max_dtv );

    for( packets = (size_t)0; !must_quit(); ++packets ) {
        (void) gettimeofday( &tva, NULL );
        nsent = 0;

        nread = read(in_fd, buf, sizeof(buf) );
        if( nread > 0 ) {
            sum_read += nread;

            if (out_fd == msockfd) {
                nsent = sendto(out_fd, buf, nread, 0, outaddr, sizeof(struct sockaddr_in));
            }
            else {
                nsent = write(out_fd, buf, nread);
            }
            if (nsent <= 0) {
                rc = errno;
                perror("sendto/write");
                break;
            }
            assert(nsent == nread);
        }

        (void) gettimeofday( &tvb, NULL );

        if( tvb.tv_usec >= tva.tv_usec ) {
            dtv.tv_usec = tvb.tv_usec - tva.tv_usec;
            dtv.tv_sec = tvb.tv_sec - tva.tv_sec;
        }
        else {
            dtv.tv_usec = tvb.tv_usec - tva.tv_usec + 1000000;
            dtv.tv_sec = tvb.tv_sec - tva.tv_sec - 1;
        }

        if( !timerisset( &max_dtv ) ) {
            min_dtv = max_dtv = dtv;
            /* TRACE( (void)fprintf( g_flog, "Reading ...\n" ) ); */
        }

        if( tvalcmp( &dtv, &min_dtv ) < 0 ) min_dtv = dtv;
        if( tvalcmp( &dtv, &max_dtv ) > 0 ) max_dtv = dtv;

#define REPORT_PERIOD_SEC 2
        tmnow = time(NULL);
        if( difftime( tmnow, ltm ) > (double)REPORT_PERIOD_SEC )
        {
            strncpy( str_tmnow, ctime(&tmnow), sizeof(str_tmnow) - 1 );
            str_tmnow[ sizeof(str_tmnow) - 1 ] = 0;
            len = strlen( str_tmnow );
            if( '\n' == str_tmnow[ len - 1 ] )
                str_tmnow[ len - 1 ] = 0;

            bytesec = sum_read / difftime(tmnow, ltm);

            (void) fprintf( g_flog, "%s - packets: %ld, bytes: %ld speed: %.2f K/sec "
                    "min: %lds.%ldms max: %lds.%ldms last: %lds.%ldms\n",
                    str_tmnow, (long)packets, (long)sum_read,
                    ((double)bytesec / 1024),
                    (long)min_dtv.tv_sec, (long)(min_dtv.tv_usec / 1000),
                    (long)max_dtv.tv_sec, (long)(max_dtv.tv_usec / 1000),
                    (long)dtv.tv_sec, (long)(dtv.tv_usec / 1000) );

            ltm = tmnow;
            packets = sum_read = (size_t)0;
            timerclear( &max_dtv );
            timerclear( &min_dtv );
        }

        if( nread <= 0 ) {
            if( EINTR == errno ) {
                (void) fprintf( g_flog, "\nINTERRUPT!\n" );
                break;
            }
            else if( EAGAIN == errno ) {
                (void) fprintf( g_flog, "\nTIME-OUT!\n" );
                rc = RC_TIMEOUT;
                break;
            }

            rc = -1;
            break;
        }

    } /* for */

    TRACE( (void)fprintf( g_flog, "%s finished\n", __func__ ) );
    return rc;
}


int
main( int argc, char* const argv[] )
{
    static const char app[] = "udpipe";
    int msockfd = -1, usockfd = -1, rc = 0, tries = -1, read_mcast = 0;
    const char  *mifaddr = NULL, *maport = NULL, *uaport = NULL;
    struct sigaction qact, oldact;
    struct sockaddr_in outaddr;

    if( argc < 4 ) {
        (void) fprintf( stderr,
                "Usage: %s mcast_ifc_addr src_ip:port dst_ip:port\n"
                "Example:\n"
                "\t %s 192.168.1.1 224.0.2.26:1234 10.0.1.23:4040\n\n",
                app, app );
        return 1;
    }

    g_flog = stdout;

    mifaddr = argv[1];
    if (is_multicast_addr(argv[2])) {
        maport = argv[2];
        uaport = argv[3];

        (void) parse_aport(uaport, &outaddr);
        read_mcast = 1;
    }
    else {
        uaport = argv[2];
        maport = argv[3];

        (void) parse_aport(maport, &outaddr);
        read_mcast = 0;
    }

    qact.sa_handler = handle_quitsigs;
    sigemptyset(&qact.sa_mask);
    qact.sa_flags = 0;

    if( (sigaction(SIGTERM, &qact, &oldact) < 0) ||
        (sigaction(SIGQUIT, &qact, &oldact) < 0) ||
        (sigaction(SIGINT,  &qact, &oldact) < 0) ||
        (sigaction(SIGPIPE, &qact, &oldact) < 0)) {
        perror("sigaction-quit");

        return 2;
    }

    usockfd = setup_udp_socket(uaport, read_mcast ? 0 : 1);
    if (usockfd < 1)
        return -1;

    msockfd = setup_mcast_socket(mifaddr, maport, read_mcast);
    if( msockfd < 1 )
        return -1;

#define TMOUT_SEC 2
    rc = set_rtmout( msockfd, TMOUT_SEC, 0 );

#define RENEW_TRIES 3
    for( tries = RENEW_TRIES; (0 == rc) && (tries >= 0); ) {
        rc = relay(msockfd, usockfd, read_mcast, (const struct sockaddr*)&outaddr);
        if( 0 != rc ) {
            if( RC_TIMEOUT == rc && (tries > 0) ) {
                rc = renew_mcast_subscr( msockfd, mifaddr );
                if( 0 != rc ) break;
                --tries;
            }
        } /* 0 != rc */

        if( must_quit() )
            break;
    }

    close_mcast_socket( msockfd, mifaddr );
    (void) close(usockfd);

    return rc;
}


/* __EOF__ */

