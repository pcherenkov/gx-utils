/* @(#) multicast-traffic bridge utility
 */

#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <net/if.h>
#include <netinet/in.h>
#include <sys/time.h>
#include <signal.h>

#ifdef _DEBUG
    #define TRACE( expr ) expr
#else
    #define TRACE( expr ) ((void)0)
#endif

#define IPv4_STRLEN     20
#define PORT_STRLEN     6

#define ETHERNET_MTU 1500
#define RC_TIMEOUT   -100

static volatile sig_atomic_t g_quit = 0;

/* return 1 if the application must gracefully quit
 */
static sig_atomic_t
must_quit() { return g_quit; }

static FILE* g_flog = NULL;

/* handler for signals requestin application exit
 */
static void
handle_quitsigs(int signo)
{
    g_quit = (sig_atomic_t)1;
    (void) &signo;

    TRACE( (void)fprintf( g_flog,
                "*** Caught SIGNAL %d in process=[%d] ***\n",
                signo, getpid()) );
    return;
}


static int
parse_aport(const char *aport, struct sockaddr_in *sa)
{
    char ipaddrp[ IPv4_STRLEN + PORT_STRLEN ] = { 0 },
         *p = NULL;
    int port = 0;

    memset(sa, 0, sizeof(*sa));
    sa->sin_family = AF_INET;

    (void) strncpy(ipaddrp, aport, sizeof(ipaddrp) - 1);
    if( NULL == (p = strtok(ipaddrp, ":") ))
        return -1;

    if( 0 == inet_aton(p, &sa->sin_addr) )
        return -1;

    if( NULL == (p = strtok( NULL, ":" )) )
        return -1;

    port = atoi(p);
    if( port < 1 ) {
        (void) fprintf(g_flog, "Invalid port value=[%d]\n",
                (int)port);
        return -1;
    }

    sa->sin_port = htons((short)port);
    return 0;
}


int
main( int argc, char* const argv[] )
{
    static const char app[] = "toudp";
    int sockfd = -1, rc = 0;
    const char *aport = NULL;
    struct sigaction qact, oldact;
    struct sockaddr_in outaddr;
    FILE *infp = NULL;

    static char line[1440];

    if( argc < 2 ) {
        (void) fprintf( stderr,
                "Usage: %s dst_ip:port [in-file]\n"
                "Example:\n"
                "\t %s 224.0.2.26:1234\n\n",
                app, app );
        return 1;
    }

    g_flog = stdout;
    infp = stdin;

    aport = argv[1];
    (void) parse_aport(aport, &outaddr);

    if (argc > 2) {
        infp = fopen(argv[2], "r");
        if (!infp) {
            perror("fopen");
            exit(2);
        }
    }

    qact.sa_handler = handle_quitsigs;
    sigemptyset(&qact.sa_mask);
    qact.sa_flags = 0;

    if( (sigaction(SIGTERM, &qact, &oldact) < 0) ||
        (sigaction(SIGQUIT, &qact, &oldact) < 0) ||
        (sigaction(SIGINT,  &qact, &oldact) < 0) ||
        (sigaction(SIGPIPE, &qact, &oldact) < 0)) {
        perror("sigaction-quit");

        return 2;
    }

    sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (sockfd < 0) {
        perror( "socket" );
        rc = -1; goto done;
    }

    fprintf(g_flog, "Input: %s\n", stdin == infp ? "STDIN" : argv[2]);

    while (NULL != fgets(line, sizeof(line)-1, infp)) {
        size_t slen = strlen(line);
        if ('\n' == line[slen-1]) {
            slen--;
            line[slen] = 0;
        }

        ssize_t nsent = sendto(sockfd, line, slen, MSG_DONTWAIT,
                    (struct sockaddr*)&outaddr, sizeof(struct sockaddr_in));
        if (nsent <= 0) {
            perror("sendto");
            break;
        }

        fprintf(g_flog, ">> [%s], %zd bytes\n", line, nsent);

        if (must_quit())
            break;
    }
    fprintf(g_flog, "Done\n");

done:
    if (infp != stdin) {
        fclose(infp);
        infp = NULL;
    }

    if (sockfd >= 0) {
        (void) close(sockfd);
    }

    return rc;
}


/* __EOF__ */

