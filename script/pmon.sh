#!/bin/sh

osid=`uname -s`

error()
{
  echo "$0: $1" >&2; exit 1
}

[ $# -lt 1 ] && error "Usage: $0 {pid} [sleep_seconds]"

_pid=$1
sleep_seconds=$2
logfile=pmon_${_pid}_"`date +"%H%M%S"`".log

trap 'QUIT=1; trap 2 3' 2 3

_sz=-1
changes=0

echo_opt=
case "$osid" in
'FreeBSD') echo_opt='-e' ;;
esac


tsmp=`date +"%m-%d-%Y %H:%M:%S"`
msg="$0: ${tsmp} Began monitoring process # ${_pid}."
echo $msg; echo $msg >${logfile}

while :
do
  ps -p $_pid >/dev/null 2>&1 || break
  new_sz=`ps -o rss -p ${_pid} | sed -n '2p'`
  if [ "$new_sz" != "$_sz" ]; then
    changes=`expr $changes + 1`

    tsmp=`date +"%m-%d-%Y %H:%M:%S"`
    msg="$0: ${tsmp} [$changes]\tsize = $new_sz"
    echo $echo_opt "$msg"; echo $echo_opt "$msg" >>${logfile}

    _sz=$new_sz
  fi

  sleep ${sleep_seconds:=1}

  if [ $QUIT ]; then
    break
  fi
done

tsmp=`date +"%m-%d-%Y %H:%M:%S"`
msg="$0: ${tsmp} Finished monitoring process # ${_pid}."
echo $msg; echo $msg >>${logfile}

case $changes in
0)  echo "$0: no records - removing ${logfile}" >&2
    rm ${logfile}
    ;;
esac
